#!/bin/sh

export DEBIAN_FRONTEND=noninteractive
export APT_LISTCHANGES_FRONTEND=none
export OSSEC_ACTION_CONFIRMED=y

# Mark etc as safe directory for git
git config --global --add safe.directory /etc

# Secure built-in admin user
passwd -d admin
passwd -l admin

# Modify system environment
chmod 644 /etc/krb5.conf
chmod 644 /etc/network/interfaces
chmod 644 /etc/network/interfaces.new
chmod 644 /etc/network/interfaces.d/*
chmod -R +rx /etc/network/if-up.d/*
chmod -R +rx /etc/network/if-pre-up.d/*
chmod -R +rx /etc/network/if-post-down.d/*
chmod -R +rx /etc/network/if-down.d/*
chmod +rx /etc/environment
chmod +x /etc/profile.d/*
chmod -R g+r,o+r /etc/hosts
chmod -R g+r,o+r /etc/nsswitch.conf
chmod -R g+r,o+r /etc/resolv.conf
chmod -R g+r,o+r /etc/ntp.conf
chmod -R g+r,o+r /etc/systemd/timesyncd.conf
chmod -R g+r,o+r /etc/chrony/chrony.conf

# Set correct permissions on service defaults
chmod -R +r /etc/default/*

# Update grub and kernel params
update-grub2

# Update CMDB client
cp /opt/cdstack-cmdb-deploy/files/usr/sbin/cmdb-deploy /usr/sbin/cmdb-deploy
chown root:root /usr/sbin/cmdb-deploy
chown root:root /usr/sbin/cmdb-deploy-dev
chmod 770 /usr/sbin/cmdb-deploy
chmod 770 /usr/sbin/cmdb-deploy-dev

# Set cron owner
chown root:root /etc/cron.d/codeorange-cmdb
chmod 0644 /etc/cron.d/codeorange-cmdb

# APT: Set owner and sync packages if enabled
chown -R _apt:root /etc/apt
chmod -R +r /etc/apt/apt.conf.d
chmod +r /etc/apt/sources.list
chmod -R +r /etc/apt/sources.list.d
chmod -R +r /etc/apt/preferences.d
chmod +r /etc/apt/trusted.gpg
chmod -R +r /etc/apt/trusted.gpg.d
{% if cmdb_host.pkg_sync_enable == 1 %}
# Sync packages from image
apt-get update --allow-releaseinfo-change
apt-get install --fix-broken --assume-yes
apt-get purge --yes --force-yes $(cat /etc/cmdb/apt_packages_purge.txt)
apt-get install --yes -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --force-yes --no-upgrade --ignore-missing $(cat /etc/cmdb/apt_packages.txt)
{% endif %}

# Reload Service Config
systemctl daemon-reload

# Disable cloud-init
systemctl disable cloud-init
systemctl mask cloud-init

# WAZUH/OSSEC - security monitoring solution for threat detection, integrity monitoring, incident response and compliance
{% if monitoring_wazuh_agent_key %}
/var/ossec/bin/manage_agents -i "{{ monitoring_wazuh_agent_key }}"
chown -R ossec:ossec /var/ossec/
systemctl enable wazuh-agent.service
{% endif %}

# Basic System Services
chown -R mail:mail /etc/dma/
chmod +r /etc/postfix/master.cf
chmod +r /etc/postfix/main.cf
chmod 644 /etc/mailname
chmod 644 /etc/aliases
systemctl enable zramswap
systemctl enable haveged
systemctl enable sshd
systemctl enable ntp || systemctl enable chrony
systemctl enable lldpd
systemctl enable snmpd

# System Monitoring
chown -R nagios:nagios /etc/icinga2
chown -R nagios:nagios /var/lib/icinga2
systemctl enable icinga2
systemctl enable salt-minion
systemctl enable smartd
systemctl enable watchdog
systemctl disable corosync
systemctl disable pacemaker
sensors-detect --auto

# Emergency notification / SMTP forward
echo "set dma/mailname {{ node_hostname_fqdn }}" | debconf-communicate
{% if smtp_forward_server_enabled == 'true' %}
chmod 600 /etc/postfix/sasl_password
postmap hash:/etc/postfix/sasl_password || true
postconf -e 'smtp_sasl_auth_enable = yes' || true
postconf -e 'smtp_sasl_security_options = noanonymous' || true
postconf -e 'smtp_sasl_password_maps = hash:/etc/postfix/sasl_password' || true
postconf -e 'smtp_tls_security_level = may' || true
echo "set dma/relayhost {{ smtp_forward_server }}" | debconf-communicate
{% endif %}

# Generate DH
if [ -f "/etc/nginx/dhparam.pem" ]
then
    echo "DH Param found."
else
    echo "DH Param not found, generating..."
    openssl dhparam -out /etc/nginx/dhparam.pem 4096
fi

# Get certs
cp /etc/code-orange/whstack-backend-client.conf /opt/whstack-backend-client/.env
/bin/bash /opt/whstack-backend-client/install.sh
/bin/bash /opt/whstack-backend-client/whstack_client.sh

# Setup KDC-Proxy
systemctl enable nginx
chmod +x /opt/code-orange/netstack-kdcproxy/pkg/build-scripts/install_pkg.sh
(cd /opt/code-orange/netstack-kdcproxy/ && /opt/code-orange/netstack-kdcproxy/pkg/build-scripts/install_pkg.sh)
systemctl enable netstack-kdcproxy-uwsgi

# Filesystem Maintenance
systemctl enable fstrim.timer

# VM-Guest Tools
systemctl enable qemu-guest-agent

# Security and Firewall
systemctl unmask shorewall
systemctl enable shorewall
systemctl unmask shorewall6
systemctl enable shorewall6

{% if fail2ban_apps %}sh /usr/local/sbin/fail2ban_touch_logs.sh{% endif %}
systemctl unmask fail2ban
systemctl enable fail2ban
systemctl enable crowdsec.service
systemctl enable crowdsec-firewall-bouncer.service


etckeeper commit "cmdb-deploy-specific-post" || true
systemctl enable etckeeper.service etckeeper.timer
